# Install

1. Clone:

        git clone https://git.duniter.org/nodes/typescript/modules/gva-api
    
2. Compile (using NodeJS 9):

        yarn
    
3. Start

        node index.js direct_start --gva-host localhost
    
Then access GVA API at http://localhost:15000/graphql.
    
    